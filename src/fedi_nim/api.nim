import jsony, json
import asyncdispatch
import httpclient
import strformat

type
  BaseFediClient = object of RootObj
    baseUrl*: string
  AsyncFediClient* = object of BaseFediClient
    hc*: AsyncHttpClient
  FediClient* = object of BaseFediClient
    hc*: HttpClient

  FediError* = object of Defect
    responseCode*: HttpCode
    info*: JsonNode

func newFediError*(respCode: HttpCode, info: JsonNode): ref FediError =
  result = newException(FediError, "")
  result.responseCode = respCode
  result.info = info

template castError(res: Response) =
  if not res.code.is2xx:
    raise newFediError(res.code, res.body.parseJson)

template castError(res: AsyncResponse) =
  if not res.code.is2xx:
    raise newFediError(res.code, (await res.body).parseJson)

proc newFediClient*(host: string, token = ""): FediClient =
  var client = newHttpClient()
  client.headers = newHttpHeaders({ "Content-Type": "application/json" })
  if token.len > 0:
    client.headers = newHttpHeaders({ "Authorization": fmt"Bearer {token}" })
  FediClient(hc: client, baseUrl: host)

proc newAsyncFediClient*(host: string, token = ""): AsyncFediClient =
  var client = newAsyncHttpClient()
  client.headers = newHttpHeaders({ "Content-Type": "application/json" })
  if token.len > 0:
    client.headers = newHttpHeaders({ "Authorization": token })
  AsyncFediClient(hc: client, baseUrl: host)

proc makeUrl(self: AsyncFediClient or FediClient, endpoint: string): string =
  result = fmt"{self.baseUrl}/{endpoint}"

proc verifyAccountCreds*(client: FediClient or AsyncFediClient): Future[JsonNode] {.multisync.} =
  let url = client.makeUrl("/api/v1/accounts/verify_credentials")
  let req = await client.hc.get(url)
  castError req
  return (await req.body).parseJson

proc getAccountInfo*(client: FediClient or AsyncFediClient, accountId: int): Future[JsonNode] {.multisync.} =
  let url = client.makeUrl(fmt"/api/v1/accounts/{accountId}")
  let req = await client.hc.get(url)
  castError req

  return (await req.body).parseJson

proc getTimeline*(client: FediClient or AsyncFediClient, local = false, remote = false, limit = 40): Future[JsonNode] {.multisync.} =
  let url = client.makeUrl("/api/v1/timelines/public")
  let req = await client.hc.get(url)
  castError req

  return (await req.body).parseJson


proc getStatuses*(client: FediClient or AsyncFediClient, accountId: int): Future[JsonNode] {.multisync.} =
  let url = client.makeUrl(fmt"/api/v1/accounts/{accountId}/statuses")
  let req = await client.hc.get(url)
  castError req
  return (await req.body).parseJson

proc getFollowers*(client: FediClient or AsyncFediClient,  accountId: int): Future[(HttpHeaders, JsonNode)] {.multisync} =
  ## Returnes a named tuple
  ## if there is pagination check the next field
  ## json data is in the data field
  let url = client.makeUrl(fmt"/api/v1/accounts/{accountId}/followers")
  let req = await client.hc.get(url)
  castError req
  return (next: req.headers, data: (await req.body).parseJson)
